<html>

<head>

<style>
body { 
   background-image: url("images/tlo_pralnia.png");
   background-repeat: no-repeat; 
   background-size: cover;
}
</style>

</head>

<body>

<?php

include 'functions.php';

if(isset($_POST["reg"])) {
$str = $_POST["reg"];

$preg = "([0-9]{8}-[0-9]{2}:[0-9]{2}-[258])";

	if(! preg_match($preg, $str) )
	{
		header("Location: index.php");
		die();
	}

$ip = $_SERVER['REMOTE_ADDR'];

$date = explode("-",$str)[0];

$curdate = date('Ymd');
$time = strtotime($curdate);

$lastMonday = strtotime('last monday', $time);
$nextSunday = strtotime('next sunday', $time);

$lastMonday = date('Ymd',$lastMonday);
$nextSunday = date('Ymd',$nextSunday);

if($date > $nextSunday)
{
	header("Location: index.php");
	die();
}
elseif($date < $lastMonday)
{
	header("Location: index.php");
	die();
}
elseif($date < $curdate)
{
	header("Location: index.php");
        die();
}

$hour = explode("-",$str)[1];
$laundry = explode("-",$str)[2];
$room = extractRoom($ip);
$conn = connect();

$query = "select * from zapisy where godzina='$hour' and dzien='$date' and pralnia='$laundry';";

$res = $conn->query($query);

if($res->num_rows > 0)
{
$conn->close();
header("Location: index.php");
die();
}
$query = "select count(*) from zapisy where pokoj='$room'";
$day = date('D');

if($day === 'Sun')
{
$day = date('Y-m-d');
$query = "select count(*) from zapisy where pokoj='$room' and dzien>'$day'";
}

$res = $conn->query($query);
$row = $res->fetch_assoc();
$usage = $row["count(*)"];

if($usage >= 2)
{
$conn->close();
header("Location: pralnia$laundry.php");
die();
}

$query = "insert into zapisy values ('".$hour."','".$date."','".$room."','".$laundry."')";
$conn->query($query);
$conn->close();

echo '

<div align="center">
<textarea readonly rows="10" cols="50" style="font-size: 20px; color: red; text-align:center; background: rgba(255,255,255,0.7);">
Poprawnie zarejestrowano.
W przypadku blednego numeru pokoju, skontaktuj się z administratorem sieci jak najszybciej.
Zostaniesz automatycznie przekierowany.

Properly registered.
In case of wrong room number, visit network administrator ASAP.
You will be automatically redirected.</textarea>
</div>

';

logToFile("zarejestrowany: $date $hour $room $laundry");

header( "refresh:6;url=pralnia$laundry.php" );

}

if(!isset($_POST["reg"])) {
	header( "location: index.php" );
}

?>

</body>
</html>
