# Prosty system rejestracji elektronicznej na pralnię. #

## W mojej sieci akademickiej, w adresie **IP** zawiera się numer pokoju. ##
Dla przykładu:

*192.168.9.15 = 915*

*192.168.82.20 = 820* (drugie urzadzenie w pokoju)

*192.168.10.20 = 1020*

*192.168.102.15 = 1015* (drugie urzadzenie na 10 pietrze)


Z czasem system zostanie zmieniony i wyodrębnianie numeru pokoju będzie inne.

Wtedy trzeba będzie nadpisać funkcję extractRoom w pliku functions.php

## W pliku db.cfg znajdują się dane logowania do bazy danych. ##

*Host*

*Username*

*Password*

*Database*

*Port*


## Tabele w bazie wyglądają następująco: ##
![zapisy.PNG](https://bitbucket.org/repo/koR95R/images/3553096066-zapisy.PNG)

![ban_pralnia.PNG](https://bitbucket.org/repo/koR95R/images/1320980602-ban_pralnia.PNG)

![admin_pralnia.PNG](https://bitbucket.org/repo/koR95R/images/2389027963-admin_pralnia.PNG)