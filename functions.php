<?php

function connect()
{
$file = fopen("db.cfg", "r") or die("Unable to open file!");
$text = fread($file,filesize("db.cfg") );
fclose($file);
$text = explode("\n",$text);

$servername = $text[0];
$username = $text[1];
$password = $text[2];
$database = $text[3];
$port = $text[4];


$conn = mysqli_connect($servername, $username, $password, $database,$port);

if (!$conn) {
echo "Failed to connect!</br>"; 
die();
}
else
{
return $conn;
}
}

function removeOldData($data)
{
	$conn = connect();
	$query = "delete from zapisy where dzien < '$data'";
	$conn->query($query);
	$conn->close();
}

function logToFile($msg)
{
	$ip = $_SERVER['REMOTE_ADDR'];
	$data = date('Y-m-d H:i:s');
	$file = fopen("usage.log", "a+");
	flock($file,LOCK_EX,$wouldblock);
	fwrite($file,$ip);
	fwrite($file,"\r\n");
	fwrite($file,$data);
	fwrite($file,"\r\n");
	fwrite($file,$msg);
	fwrite($file,"\r\n");
	fwrite($file,"*******************");
	fwrite($file,"\r\n");
	flock($file,LOCK_UN);
	fclose($file);
}

function checkUsage($ip,$laundry,$from)
{
$conn = connect();
$room = extractRoom($ip);
$to = date( 'Ymd', strtotime( $from .' + 6 day') );

$query = "select * from zapisy where pokoj='".$room."' and dzien between '".$from."' and '".$to."'";;
$result = $conn->query($query);

return $result->num_rows;
}

function extractRoom($ip)
{
$first = explode(".",$ip)[2];
$second = explode(".",$ip)[3];

$length = strlen($first);
$seclen = strlen($second);

if($length == "1" and $seclen > 1)
{
return $first."".$second;
}
else if($length == "1" and $seclen == "1")
{
return $first."0".$second;
}
else if($length == "2" and $seclen == "1")
{
	if($first == "10")
	{
	return $first."0".$second;	
	}
	else
	{
	$first= substr($first,0,-1);
	return $first."0".$second;
	}
}
else if($length == "2")
{

	if($first == "10")
	{
	return $first."".$second;	
	}
	else
	{
	$first= substr($first,0,-1);
	return $first."".$second;	
	}
}
else if($length == "3" and $seclen == "1")
{
	$first= substr($first,0,-1);
	return $first."0".$second;		
}
else if($length == "3")
{
	$first= substr($first,0,-1);
	return $first."".$second;		
}

}

function testdate()
{

$file = fopen("laundry.cfg", "r+") or die("Unable to open file!");
$from = fgets($file);
$to = fgets($file);
fclose($file);
$cur = date('Ymd');

$tommorow = date('Ymd', strtotime( date( 'Ymd') .' + 1 day') );

preg_match('/([0-9]+)/',$to,$arr);
$to = $arr[0];
preg_match('/([0-9]+)/',$from,$arr);
$from = $arr[0];
	
if($to == $tommorow && date('H:i') > '18:15' )
{

$from = date( 'Ymd', strtotime( date( 'Ymd') .' + 1 day') );
$to = date( 'Ymd', strtotime( date( 'Ymd') .' + 8 day') );
$file = fopen("laundry.cfg", "w+") or die("Unable to create file!");

fwrite($file,$from);
fwrite($file,"\r\n"); 
fwrite($file,$to);

fclose($file);
removeOldData($cur);
}

if($cur >= $to)
{
$from = date('Ymd');
$to = date( 'Ymd', strtotime( date( 'Ymd') .' + 7 day') );
$file = fopen("laundry.cfg", "w+") or die("Unable to create file!");

fwrite($file,$cur);
fwrite($file,"\r\n"); //*********************************** ZMIENIC NA LINUXIE NA /n !!!!!!!!!!!!!!!!!!
fwrite($file,$to);

fclose($file);

removeOldData($from);
}

if($cur == $from)
{
removeOldData($from);
}


return date( 'Y-m-d', strtotime($from) );
}

function testAvailibity($laundry,$ip)
{

$conn = connect();
$room = extractRoom($ip);
$query = "select pralnia from ban_pralnia where pokoj='".$room."'"; 

$result = $conn->query($query);
$conn->close();
if($result->num_rows > 0)
{
	while ($row = $result->fetch_assoc()) 
	{
	if($row["pralnia"] == $laundry)
		{
		return true;
		}
	}
}
return false;
}

function makeLabels($hour,$date,$pokoj,$conn,$laundry,$style)
{
	
        for ($i=0;$i<7;$i++)
        {
        $query = "select pokoj from zapisy where godzina='$hour' and pralnia='$laundry' and dzien=".$date;
        $result = $conn->query($query);
		
        if ($result->num_rows > 0) {

                $row = $result->fetch_assoc();
                $room = $row["pokoj"];
                if($room == $pokoj)
                {

			if(date('Ymd') <= $date && date('H:i') < $hour )
			{

                        echo '
						<form action="erase.php" method="post" onsubmit="return TestUser()">
						<th class="WHITE">						
						<input class="WHITE" title="Click to delete entry" type="submit" name="resign" value="'.$room.' (cancel)"></input>
						<input type="hidden" name="delhour" value="'.$date.",".$hour.",".$pokoj.",".$laundry.'"></input>
						</th>
						</form>';
                	}
			
			elseif(date('Ymd') < $date )
                        {

                        echo '<th class="WHITE">
                                                <form action="erase.php" method="post" onsubmit="return TestUser()">
                                                <br>
                                                <input class="WHITE" title="Click to delete entry" type="submit" name="resign" value="'.$room.' (cancel)"></input>
                                                <input type="hidden" name="delhour" value="'.$date.",".$hour.",".$pokoj.",".$laundry.'"></input>
                                                </form>
                                                </th>';
                        }


			else
			{
			echo '<th class="'.$style.'">'.$room.'</th>';	
			}

		}

                else
                {
                        echo '<th class="'.$style.'">'.$room.'<br></th>';
                }
        }

                else
                {
                        global $usage;

                        if($usage >= 2)
                        {
                                echo '
                                <th class="'.$style.'">
				<img src="images/cant.png" title="next list will be available on sunday at 18:15" width="70"></input>
                                </th>
                                ';
                        }
                        else
                        {
				if(date('Ymd') <= $date && date('H:i') < $hour )
				{

                                echo '
                                <th class="'.$style.'">
                                <form action="register.php" method="post">
				<br>
				<input type="image" src="images/reg.png" width="65%"></input>
                                <input type="hidden" name="reg" value="'.$date."-".$hour."-".$laundry.'"></input>
                                </form>
                                </th>
                                ';
				}

				else if(date('Ymd') < $date )
				{
                                echo '
                                <th class="'.$style.'">
                                <form action="register.php" method="post">
                                <br>
                                <input type="image" src="images/reg.png" width="65%"></input>
                                <input type="hidden" name="reg" value="'.$date."-".$hour."-".$laundry.'"></input>
                                </form>
                                </th>
                                ';

				}

				else
				{
                                echo '
                                <th class="'.$style.'">
				<img src="images/cant.png" title="next list will be available on sunday at 18:15" width="70"></input>
                                </th>
                                ';
				}

                        }
                }
        $date = date( 'Ymd', strtotime( $date .' + 1 day') );
        }
}

function createTable($date,$laundry,$pokoj)
{
$conn = connect();
$tableDate = $date;
echo '

<style>


table,th {
    border: 1px solid black;
    text-align: center;
    vertical-align:middle;
    font-size: 110%;
    background: rgba(255,255,255,0.65);
    table-layout: fixed;
    height: 70px;
}


th.BLUE {
    border: 1px solid blue;
}
th.RED {
    border: 1px solid red;
}
th.GREEN {
    border: 1px solid green;

}
th.PINK {
    border: 1px solid pink;
}
th.WHITE {
    border: 1px solid black;
    background-color: black;
    color: red;
}
input.WHITE {
	border: 1px solid black;
	background-color: black;
	color: red;
	font-size: 23px;
	white-space: normal;
}



</style>

<div align="left">

<table width="100%" rowspacing="10">

<tr>
        <th>
        <pre>   Data ->		
Godzina&#9660;</pre>
        </th>
        ';
        for ($i=0;$i<7;$i++)
        {
        echo '<th>'.$tableDate.'</th>';
        $tableDate = date( 'd.m.Y', strtotime( $tableDate .' + 1 day') );
        }
        echo'
</tr>
<tr>
        <th>
        8:00 - 10:00
        </th>
        ';
        $tableDate = date('Ymd',strtotime($date) );
        makeLabels("08:00",$tableDate,$pokoj,$conn,$laundry,"BLUE");
        echo '
</tr>
<tr>
        <th>
        10:00 - 12:00
        </th>
        ';
        $tableDate = date('Ymd',strtotime($date) );
        makeLabels("10:00",$tableDate,$pokoj,$conn,$laundry,"RED");
        echo '
</tr>
<tr>
        <th>
        12:00 - 14:00
        </th>
        ';
        $tableDate = date('Ymd',strtotime($date) );
        makeLabels("12:00",$tableDate,$pokoj,$conn,$laundry,"GREEN");
        echo '
</tr>
<tr>
        <th>
        14:00 - 16:00
        </th>
        ';
        $tableDate = date('Ymd',strtotime($date) );
        makeLabels("14:00",$tableDate,$pokoj,$conn,$laundry,"PINK");
        echo '
</tr>
<tr>
        <th>
        16:00 - 18:00
        </th>
        ';
        $tableDate = date('Ymd',strtotime($date) );
        makeLabels("16:00",$tableDate,$pokoj,$conn,$laundry,"BLUE");
        echo '
</tr>
<tr>
        <th>
        18:00 - 20:00
        </th>
        ';
        $tableDate = date('Ymd',strtotime($date) );
        makeLabels("18:00",$tableDate,$pokoj,$conn,$laundry,"RED");
        echo '
</tr>

<tr>
        <th>
        20:00 - 22:00
        </th>
        ';
        $tableDate = date('Ymd',strtotime($date) );
        makeLabels("20:00",$tableDate,$pokoj,$conn,$laundry,"GREEN");
        echo '
</tr>
</table>
</div>
';
$conn->close();
}

function checkSession()
{
		$login_session_duration = 300;
                $current_time = time();
                if(isset($_SESSION['loggedin_time']) )
                {
                if(((time() - $_SESSION['loggedin_time']) > $login_session_duration)){
                        session_destroy();
                        header("Location: login.php");
                }
                }

}


?>

<script type="text/javascript">

function TestUser(Message) {
	
	if(Message == undefined)
	{
	if (confirm('Are you sure you want to that?')) {
    return true;
	} else {
    return false;
    }
	}
	else
	{
    if (confirm(Message)) {
    return true;
	} else {
    return false;
    }
	}
}

</script>
